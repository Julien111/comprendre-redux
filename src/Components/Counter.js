import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useState, useEffect} from 'react';
import {getCatImg} from '../redux/Reducer/DataImgReducer';

export default function Counter() {

    const [cartData, setCartData] = useState(0);

    const {cart, count, imgUrl} = useSelector(state => ({
        ...state.AddCartReducer,
        ...state.CounterReducer,
        ...state.DataImgReducer,
    }));

    const dispatch = useDispatch(); //ce hook va dispatcher le state

    const decreFunc = () =>{
        dispatch({
            type: "DECR"
        })
    }

    const increFunc = () => {
      dispatch({
        type: "INCR",
      });
    };

    const addToCartFunc= () => {
            dispatch({
                type: "ADDCART",  //ici on récupère les datas de l'input dans cartData et on les passe à payload
                payload: cartData
            })
    }

    useEffect(() => {
      dispatch(getCatImg());
    }, [])


    return (
      <div>
        <h1>Votre panier {cart}</h1>
        <h2>Votre compteur {count}</h2>

     
        <button onClick={decreFunc}>-1</button>
        <button onClick={increFunc}>+1</button>
        <br />

        {/* ici onInput récup les datas de l'input et les transfère à setCartData */}
        <input
          value={cartData}
          onInput={(e) => setCartData(e.target.value)}
          type="number"
        />
        <br />
        <button onClick={addToCartFunc}>Ajouter au panier</button> 
        {/* ici le bouton va enclencher addToCartFunc et modifier le state de cart qui est affiché dans le h1 */}

        {/* Partie img async */}
        <div className="img">

          {imgUrl && <img style={{width: "300px"}} alt="image de chat" src={imgUrl} />}

        </div>

      </div>
    );
}
