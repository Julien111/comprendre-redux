const INITIAL_STATE = {
  cart: 0,
};

/** ici on crée l'objet add a cart qui va avec l'action payload
 * récupérer les données d'un input
 */

function AddCartReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "ADDCART": {
      return {
        ...state,
        cart: action.payload,
      };
    }    
  }

  return state;
}

export default AddCartReducer;
