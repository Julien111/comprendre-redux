const INITIAL_STATE = {
  imgUrl: "",
};

/** ici on crée l'objet add a cart qui va avec l'action payload
 * récupérer les données d'un input
 */

function DataImgReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "LOADING": {
      return {
        ...state,
        imgUrl: action.payload,
      };
    }
  }

  return state;
}

export default DataImgReducer;

export const getCatImg = () => dispatch => {

    fetch("https://api.thecatapi.com/v1/images/search").then(response => response.json())
    .then(data => {
        dispatch({
            type: "LOADING",
            payload: data[0].url
        })    
    })
}
