import { createStore, combineReducers, applyMiddleware } from "redux";
import CounterReducer from "../redux/Reducer/CounterReducer";
import AddCartReducer from "../redux/Reducer/AddCartReducer";
import DataImgReducer from "../redux/Reducer/DataImgReducer";
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
  CounterReducer,
  AddCartReducer,
  DataImgReducer
});

  
const store = createStore(rootReducer, applyMiddleware(thunk));



export default store;